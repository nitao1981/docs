# APIs

-   Resource Management
    -   [Resource Manager](js-apis-resource-manager.md)
    -   [Internationalization \(intl\) ](js-apis-intl.md)
    -   [Internationalization \(i18n\) ](js-apis-i18n.md)
-   Media
    -   [Audio Management](js-apis-audio.md)
    -   [Media Playback and Recording](js-apis-media.md)
-   Data Management
    -   [File Management](js-apis-fileio.md)
    -   [Lightweight Storage](js-apis-data-storage.md)
    -   [Distributed Data Management](js-apis-distributed-data.md)
    -   [Relational Database](js-apis-data-rdb.md)
    -   [Result Set](js-apis-data-resultset.md)
    -   [DataAbilityPredicates](js-apis-data-ability.md)
-   Account Management
    -   [Distributed Account Management](js-apis-distributed-account.md)
-   Telephony Service
    -   [Call](js-apis-call.md)
    -   [SMS](js-apis-sms.md)
    -   [SIM Management](js-apis-sim.md)
    -   [Radio](js-apis-radio.md)
-   Network and Connectivity
    -   [WLAN](js-apis-wifi.md)  
-   Device Management
    -   [Sensors](js-apis-sensor.md)
    -   [Vibration](js-apis-vibrator.md)
    -   [Brightness](js-apis-brightness.md)
    -   [Battery Info](js-apis-battery-info.md)
    -   [Power Management](js-apis-power.md)
    -   [Running Lock](js-apis-runninglock.md)
    -   [Device Info](js-apis-device-info.md)
    -   [systemParameter](js-apis-system-parameter.md)
    -   [Device Management](js-apis-device-manager.md)
    -   [Window](js-apis-window.md)
    -   [Display](js-apis-display.md)
    -   [Update](js-apis-update.md) 
-   Basic Features
    -   [Application Context](js-apis-basic-features-app-context.md)
    -   [Console Logs](js-apis-basic-features-logs.md)
    -   [Page Routing](js-apis-basic-features-routes.md)
    -   [Pop-up Window](js-apis-basic-features-pop-up.md)
    -   [Application Configuration](js-apis-basic-features-configuration.md)
    -   [Timer](js-apis-basic-features-timer.md)
    -   [Setting the System Time](js-apis-system-time.md)
    -   [Animation](js-apis-basic-features-animator.md)
    -   [HiAppEvent](js-apis-hiappevent.md)
    -   [Performance Tracing](js-apis-bytrace.md)
-   Language Base Class Library
    -   [Obtaining Process Information](js-apis-process.md)
    -   [URL String Parsing](js-apis-url.md)
    -   [String Encoding and Decoding](js-apis-util.md)
    -   [Worker Startup](js-apis-worker.md)

