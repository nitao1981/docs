# tab-bar<a name="ZH-CN_TOPIC_0000001173324603"></a>

<[tabs](js-components-container-tabs.md)\>的子组件，用来展示tab的标签区，子组件排列方式为横向排列。

## 权限列表<a name="section11257113618419"></a>

无

## 子组件<a name="sfddaafa400fa4802a6c4344a0cc0f9ed"></a>

支持。

## 属性<a name="s45d9533b71b049aba681c57db73d9f7b"></a>

除支持[通用属性](js-components-common-attributes.md)外，还支持如下属性：

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="23.119999999999997%" id="mcps1.1.6.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="23.119999999999997%" id="mcps1.1.6.1.2"><p>类型</p>
</th>
<th class="cellrowborder" valign="top" width="10.48%" id="mcps1.1.6.1.3"><p>默认值</p>
</th>
<th class="cellrowborder" valign="top" width="7.5200000000000005%" id="mcps1.1.6.1.4"><p>必填</p>
</th>
<th class="cellrowborder" valign="top" width="35.76%" id="mcps1.1.6.1.5"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="23.119999999999997%" headers="mcps1.1.6.1.1 "><p>mode</p>
</td>
<td class="cellrowborder" valign="top" width="23.119999999999997%" headers="mcps1.1.6.1.2 "><p>string</p>
</td>
<td class="cellrowborder" valign="top" width="10.48%" headers="mcps1.1.6.1.3 "><p>scrollable</p>
</td>
<td class="cellrowborder" valign="top" width="7.5200000000000005%" headers="mcps1.1.6.1.4 "><p>否</p>
</td>
<td class="cellrowborder" valign="top" width="35.76%" headers="mcps1.1.6.1.5 "><p>设置组件宽度的可延展性。可选值为：</p>
<ul><li>scrollable：子组件宽度为实际设置的宽度，当宽度之和（包括margin边距）大于tab-bar的宽度时，子组件可以横向滑动。</li><li>fixed：子组件宽度均分tab-bar的宽度。</li></ul>
</td>
</tr>
</tbody>
</table>

## 样式<a name="section193239416388"></a>

支持[通用样式](js-components-common-styles.md)。

## 事件<a name="section71081937192815"></a>

支持[通用事件](js-components-common-events.md)。

## 方法<a name="section2279124532420"></a>

支持[通用方法](js-components-common-methods.md)。

## 示例<a name="section10605165263911"></a>

详见[tabs示例](js-components-container-tabs.md#section14993155318710)。

