# ImageBitmap对象<a name="ZH-CN_TOPIC_0000001181948861"></a>

>![](../../public_sys-resources/icon-note.gif) **说明：** 
>从API version 7开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

ImageBitmap对象由OffscreenCanvas对象的transferToImageBitmap\(\)方法生成，存储了offscreen canvas渲染的像素数据。

## 属性<a name="section661391987"></a>

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="22.872287228722872%" id="mcps1.1.4.1.1"><p>属性</p>
</th>
<th class="cellrowborder" valign="top" width="29.352935293529352%" id="mcps1.1.4.1.2"><p>类型</p>
</th>
<th class="cellrowborder" valign="top" width="47.774777477747776%" id="mcps1.1.4.1.3"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="22.872287228722872%" headers="mcps1.1.4.1.1 "><p>width</p>
</td>
<td class="cellrowborder" valign="top" width="29.352935293529352%" headers="mcps1.1.4.1.2 "><p>number</p>
</td>
<td class="cellrowborder" valign="top" width="47.774777477747776%" headers="mcps1.1.4.1.3 "><p>ImageBitmap的像素宽度。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="22.872287228722872%" headers="mcps1.1.4.1.1 "><p>height</p>
</td>
<td class="cellrowborder" valign="top" width="29.352935293529352%" headers="mcps1.1.4.1.2 "><p>number</p>
</td>
<td class="cellrowborder" valign="top" width="47.774777477747776%" headers="mcps1.1.4.1.3 "><p>ImageBitmap的像素高度。</p>
</td>
</tr>
</tbody>
</table>

