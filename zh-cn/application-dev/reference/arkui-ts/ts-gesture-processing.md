# 手势处理<a name="ZH-CN_TOPIC_0000001192595144"></a>

-   **[绑定手势方法](ts-gesture-settings.md)**  

-   **[基础手势](ts-basic-gestures.md)**  

-   **[组合手势](ts-combined-gestures.md)**  


