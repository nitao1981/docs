# 通用事件<a name="ZH-CN_TOPIC_0000001237715071"></a>

-   **[点击事件](ts-universal-events-click.md)**  

-   **[触摸事件](ts-universal-events-touch.md)**  

-   **[挂载卸载事件](ts-universal-events-show-hide.md)**  

-   **[按键事件](ts-universal-events-key.md)**  

-   **[组件区域变化事件](ts-universal-events-component-area-change.md)**  


