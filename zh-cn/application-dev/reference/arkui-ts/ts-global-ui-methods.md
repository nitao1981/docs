# 全局UI方法<a name="ZH-CN_TOPIC_0000001237475047"></a>

-   **[警告弹窗](ts-methods-alert-dialog-box.md)**  

-   **[自定义弹窗](ts-methods-custom-dialog-box.md)**  

-   **[图片缓存](ts-methods-image-cache.md)**  

-   **[媒体查询](ts-methods-media-query.md)**  

-   **[列表选择弹窗](ts-methods-custom-actionsheet.md)**  


