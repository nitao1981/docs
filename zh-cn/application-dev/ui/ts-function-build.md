# build函数



**build**函数满足**Builder**构造器接口定义，用于定义组件的声明式UI描述。


```
interface Builder {
    build: () => void
}
```
