# UI状态管理



- **[基本概念](ts-ui-state-mgmt-concepts.md)**

- **[管理组件拥有的状态](ts-managing-component-states.md)**

- **[管理应用程序的状态](ts-managing-application-states.md)**

- **[其他类目的状态管理](ts-managing-other-states.md)**