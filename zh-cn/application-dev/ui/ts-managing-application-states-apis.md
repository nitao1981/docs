# 接口



- **[应用程序的数据存储](ts-application-states-appstorage.md)**

- **[持久化数据管理](ts-application-states-apis-persistentstorage.md)**

- **[环境变量](ts-application-states-apis-environment.md)**