# Hi3516开发板<a name="ZH-CN_TOPIC_0000001217013857"></a>

-   **[安装开发板环境](quickstart-lite-steps-hi3516-setting.md)**  

-   **[新建应用程序](quickstart-lite-steps-hi3516-application-framework.md)**  

-   **[编译](quickstart-lite-steps-hi3516-building.md)**  

-   **[烧录](quickstart-lite-steps-hi3516-burn.md)**  

-   **[运行](quickstart-lite-steps-hi3516-running.md)**  

-   **[常见问题](quickstart-lite-steps-hi3516-faqs.md)**  


