# RK3568开发板<a name="ZH-CN_TOPIC_0000001234047409"></a>

-   **[创建应用程序](quickstart-standard-running-rk3568-create.md)**  

-   **[源码编译](quickstart-standard-running-rk3568-build.md)**  

-   **[镜像烧录](quickstart-standard-running-rk3568-burn.md)**  

-   **[镜像运行](quickstart-standard-running-rk3568-run.md)**  


