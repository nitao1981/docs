# 搭建轻量与小型系统环境<a name="ZH-CN_TOPIC_0000001216535391"></a>

-   **[搭建系统环境概述](quickstart-lite-env-setup-overview.md)**  

-   **[开发环境准备](quickstart-lite-env-prepare.md)**  

-   **[获取源码](quickstart-lite-sourcecode-acquire.md)**  

-   **[使用安装包方式搭建编译环境](quickstart-lite-package-environment.md)**  

-   **[使用Docker方式搭建编译环境](quickstart-lite-docker-environment.md)**  

-   **[常见问题](quickstart-lite-env-setup-faqs.md)**  


