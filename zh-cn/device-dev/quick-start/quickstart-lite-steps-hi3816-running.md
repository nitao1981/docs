# 运行<a name="ZH-CN_TOPIC_0000001171774076"></a>

-   [运行结果](#section18115713118)
-   [下一步学习](#section9712145420182)

## 运行结果<a name="section18115713118"></a>

示例代码编译、烧录、运行、调测后，重启开发板后将自动在界面输出如下结果：

```
ready to OS start
FileSystem mount ok.
wifi init success!
[DEMO] Hello world.
```

## 下一步学习<a name="section9712145420182"></a>

恭喜，您已完成Hi3861开发板快速上手！建议您下一步进入[WLAN产品开发](../guide/device-wlan-led-control.md)的学习 。

