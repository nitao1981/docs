# HiSysEvent开发指导<a name="ZH-CN_TOPIC_0000001195021448"></a>

-   **[HiSysEvent打点指导](subsys-dfx-hisysevent-write.md)**  

-   **[HiSysEvent订阅指导](subsys-dfx-hisysevent-read.md)**  

-   **[HiSysEvent查询指导](subsys-dfx-hisysevent-select.md)**  

-   **[HiSysEvent工具使用指导](subsys-dfx-hisysevent-tool.md)**  


